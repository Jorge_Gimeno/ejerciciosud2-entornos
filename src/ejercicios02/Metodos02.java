package ejercicios02;

import java.util.Scanner;

public class Metodos02 {

	public static Scanner input = new Scanner(System.in);

	public static void nombre() {
		System.out.println("Introduce tu nombre");
		String N = input.nextLine();
		System.out.println(N);

	}

	public static void apellido() {
		System.out.println("Introduce tu apellido");
		String A = input.nextLine();
		System.out.println(A);
	}

	public static void direccion() {
		System.out.println("Introduce tu direccion");
		String D = input.nextLine();
		System.out.println(D);

	}

}
